import math


class Pizza:
    def __init__(self, radius: int, ingredients: list[str]) -> None:
        self.ingredients = ingredients
        self.radius = radius

    def __repr__(self):
        return f" Pizza({self.ingredients!r})"

    @classmethod
    def margarita(cls, radius: int):
        return cls(radius, ["mozzarella", "tomatoes"])

    @classmethod
    def prosciutto(cls, radius: int):
        return cls(radius, ["mozzarella", "tomatoes", "ham"])

    def area(self):
        return self.circle_area(self.radius)

    @staticmethod
    def circle_area(r):
        return math.pi * r * r

